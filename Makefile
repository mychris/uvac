
LISP ?= sbcl
SRC != find . -name '*.lisp'
TESTS != find . -name '*.lisp'

all: uvac

uvac: uvac.asd $(SRC)
	$(LISP) --non-interactive \
	        --eval '(require "asdf")' \
	        --eval '(asdf:load-asd (merge-pathnames "uvac.asd" (uiop:getcwd)))' \
	        --eval '(asdf:load-system :uvac)' \
	        --eval '(asdf:make :uvac)' \
	        --eval '(uiop:quit)'

test check: uvac.asd $(SRC) $(TESTS)
	$(LISP) --non-interactive \
	        --eval '(require "asdf")' \
	        --eval '(asdf:load-asd (merge-pathnames "uvac.asd" (uiop:getcwd)))' \
	        --eval '(asdf:test-system :uvac)' \
	        --eval '(uiop:quit)'

install:
	mkdir -p $(DESTDIR)$(PREFIX)/bin
	cp -f uvac $(DESTDIR)$(PREFIX)/bin
	chmod 755 $(DESTDIR)$(PREFIX)/bin/uvac

uninstall:
	rm -f $(DESTDIR)$(PREFIX)/bin/uvac

clean:
	$(RM) uvac
	$(RM) src/*.fasl src/*.FASL src/*.dfsl src/*.pfsl

.PHONY: all clean install uninstall
