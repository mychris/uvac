;;;; utils.lisp

(defpackage #:utils
  (:use #:common-lisp #:cl)
  (:export
   #:err
   #:read-stream
   #:tableizer-column
   #:make-tableizer-column
   #:tableizer
   #:make-tableizer
   #:tableizer-format))
(in-package #:utils)

(defun err (fmt &rest args)
  (format *error-output* "~A: ~A~&" (uiop:argv0) (apply #'format (append (list nil fmt) args)))
  (uiop:quit 1))

(defun read-stream (instream)
  (let ((ostream (make-string-output-stream))
	(buf (make-array 4096 :element-type (stream-element-type instream))))
    (loop for pos = (read-sequence buf instream)
        while (plusp pos)
	  do (write-sequence buf ostream :end pos))
    (get-output-stream-string ostream)))

;;;; TODO: This needs to be re-implemented! There are so many improvements that could be made.
(defclass tableizer-column ()
  ((name :reader tableizer-column-name
	 :initarg :name
	 :documentation
	 "")
   (header :reader tableizer-column-header
	   :initarg :header
	   :documentation
	   "")
   (data-extractor :reader tabelelizer-column-data-extractor
		   :initarg :data-extractor
		   :documentation
		   ""))
  (:documentation
   ""))

(defun make-tableizer-column (&rest rest)
  (apply #'make-instance 'tableizer-column rest))

(defclass tableizer ()
  ((columns :reader tableizer-columns
	    :initarg :columns
	    :documentation
	    ""))
  (:documentation
   ""))

(defun make-tableizer (columns)
  (make-instance 'tableizer :columns columns))

(defmethod tableizer-find-longest ((tbl tableizer) data)
  (let ((result (make-array (list (length (tableizer-columns tbl))) :initial-element 0)))
    (loop :for tbl-col :in (tableizer-columns tbl)
	  :for num-col :from 0 :do
	    (setf (aref result num-col) (length (tableizer-column-header tbl-col))))
    (loop :for data-col :in data :do
      (loop :for tbl-col :in (tableizer-columns tbl)
	    :for num-col :from 0 :do
	      (setf (aref result num-col)
		    (max (aref result num-col)
			 (length (or (format nil "~A" (funcall (tabelelizer-column-data-extractor tbl-col) data-col)) ""))))))
    result))

(defmethod tableizer-format ((tbl tableizer) stream data &key (show-headings t))
  (if (= 0 (length data))
      ""
      (let ((column-widths (tableizer-find-longest tbl data)))
	(when show-headings
	  (format stream "~{~v@A~^ ~}~%"
		  (loop :for tbl-col :in (tableizer-columns tbl)
			:for width :across column-widths
			:collect
			width
			:collect
			(tableizer-column-header tbl-col))))
	(format stream "~{~{~vA~^ ~}~%~}"
		(loop :for data-col :in data
		      :collect
		      (loop :for tbl-col :in (tableizer-columns tbl)
			    :for width :across column-widths
			    :collect
			    width
			    :collect
			    (or (funcall (tabelelizer-column-data-extractor tbl-col) data-col) "")))))))
