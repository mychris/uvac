;;;; cmd-accepted-output.lisp

(defpackage #:cmd-accepted-output
  (:use #:common-lisp #:cl)
  (:export
   #:make-command))
(in-package #:cmd-accepted-output)

(defun retrieve-accepted-output (pid instream)
  (let ((api (make-instance 'udebug:api))
	(instring (utils:read-stream instream)))
    (format t "~A~&" (udebug:get-accepted-output api pid instring))))

(defun command-handler (cmd)
  (when (< (length (clingon:command-arguments cmd)) 1)
    (clingon:print-usage-and-exit cmd *error-output*))
  (let ((pid (first (clingon:command-arguments cmd)))
	(input-file-name (or (second (clingon:command-arguments cmd)) "-"))
	(input nil))
    (handler-case
	(setf pid (nth-value 0 (parse-integer pid)))
      (error () (utils:err "Invalid problem ID: ~A" pid)))
    (if (string= input-file-name "-")
	(setf input uiop:*stdin*)
	(setf input (open input-file-name :direction :input :if-does-not-exist nil)))
    (when (not input)
      (utils:err "~A: No such file or directory" input-file-name))
    (retrieve-accepted-output pid input)))

(defun make-options ()
  (list
   ))

(defun make-command ()
  (clingon:make-command
   :name "accepted-output"
   :description "Retrieve the accepted ouput for a problem, using the given input."
   :options (make-options)
   :usage "[options] problem [input-file]"
   :long-description nil
   :handler #'command-handler))
