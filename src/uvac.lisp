;;;; uvac.lisp

(defpackage #:uvac
  (:use #:common-lisp #:cl))
(in-package #:uvac)

(defparameter *uvac-environment-configuration*
  '(("UVAC_HTTP_DEBUG_HEADERS" . uvac-envconf-http-headers-handler)))

(defun uvac-envconf-http-headers-handler (key value)
  (declare (ignore key))
  (when (or (string= "t" value)
	    (string= "true" value)
	    (string= "y" value)
	    (string= "yes" value)
	    (string= "1" value))
    (setf drakma:*header-stream* uiop:*stderr*)))

(defun handle-environment ()
  (loop :for conf :in *uvac-environment-configuration* :do
    (when (uiop:getenvp (car conf))
      (funcall (cdr conf) (car conf) (uiop:getenv (car conf))))))

(defun uvac-handler (cmd)
  (clingon:print-usage-and-exit cmd uiop:*stdout*))

(defun make-uvac-options ()
  (list
   ))

(defun make-uvac-command ()
  (clingon:make-command
   :name "uvac"
   :description "UVa command line client"
   :options (make-uvac-options)
   :sub-commands (list (cmd-input:make-command)
		       (cmd-accepted-output:make-command)
		       (cmd-solved:make-command)
		       (cmd-unsolved:make-command)
		       (cmd-ranklist:make-command)
		       (cmd-rank:make-command))
   :handler #'uvac-handler))

(defun main (args)
  (handle-environment)
  (let ((app (make-uvac-command)))
    (clingon:run app args)))

(defun plain-main ()
  (main (uiop:command-line-arguments)))
