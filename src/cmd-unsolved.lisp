;;;; cmd-rank.lisp

(defpackage #:cmd-unsolved
  (:use #:common-lisp #:cl)
  (:export
   #:make-command))
(in-package #:cmd-unsolved)

(defun command-handler (cmd)
  (when (not (clingon:command-arguments cmd))
    (clingon:print-usage-and-exit cmd *error-output*))
  (let* ((api (make-instance 'uhunt:api))
	 (username (first (clingon:command-arguments cmd)))
	 (uid (uhunt:uname-to-uid api username)))
    (when (= 0 uid)
      (utils:err "Unknown username: ~A" username))
    (let* ((all-submissions (uhunt:submissions api uid))
	   (tried (mapcar #'uhunt:uva-submission-pid all-submissions))
	   (solved (loop :for submission :in all-submissions
			 :when (= (uhunt:uva-submission-verdict submission) uhunt:+accepted+)
			   :collect (uhunt:uva-submission-pid submission)))
	   (unsolved-problem-pids (set-difference (delete-duplicates tried) (delete-duplicates solved)))
	   (unsolved-problems (sort
			       (remove-if-not (lambda (x) (member (uhunt:uva-problem-pid x) unsolved-problem-pids)) (uhunt:all-problems api))
			       (lambda (a b) (< (uhunt:uva-problem-num a) (uhunt:uva-problem-num b))))))
      (loop :for unsolved-problem :in unsolved-problems :do
	(format uiop:*stdout* "~A~%" (uhunt:uva-problem-num unsolved-problem))))))

(defun make-options ()
  (list
   ))

(defun make-command ()
  (clingon:make-command
   :name "unsolved"
   :description "List all problems which have been tried but not yet solved."
   :options (make-options)
   :usage "[options] username"
   :long-description nil
   :handler #'command-handler))
