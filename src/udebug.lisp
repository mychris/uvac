;;;; udebug.lisp

(defpackage #:udebug
  (:use #:common-lisp #:cl)
  (:export
   #:api
   #:list-problem-inputs
   #:get-problem-input
   #:get-accepted-output
   ))
(in-package #:udebug)

;;;; We are not using the official API, since one needs to register and I want this te be
;;;; easy to use without any setup at all,
;;;;
;;;; Getting input:
;;;;   Get the udebug html sources for a problem:
;;;;      https://www.udebug.com/UVa/<pid>   https://www.udebug.com/UVa/100
;;;;   Search for links with the css class 'input_desc'.
;;;;   Each <aref> with that class has an additional property called 'data-id'
;;;;   Make a post request to https://www.udebug.com/udebug-custom-get-selected-input-ajax
;;;;      with the data 'input_nid=<data-id>'

(defclass api ()
  ((base-url :accessor api-base-url
	     :initarg :base-url
	     :documentation
	     "Base url for the uhunt online API")
   (timeout :accessor api-timeout
	    :initarg :timeout
	    :documentation
	    "The time (in seconds) until an attempt to connect to a server is considered a failure")
   (proxy-server :accessor api-proxy-server
		 :initarg :proxy-server
		 :documentation
		 "A list of two values - a string denoting the proxy server and an integer denoting the port to use")
   (proxy-auth :accessor api-proxy-auth
	       :initarg :proxy-auth
	       :documentation
	       "A list of two strings (username and password) which will be sent to the proxy server for authorization")
   (cookie-jar :accessor api-cookie-jar
	       :initarg :cookie-jar
	       :documentation
	       "A cookie jar containing cookies which will potentially be sent to the server")
   )
  (:default-initargs
   :base-url "https://www.udebug.com"
   :timeout 5
   :proxy-server nil
   :proxy-auth nil
   :cookie-jar (make-instance 'drakma:cookie-jar)
   )
  (:documentation "Make requests to the uhunt online API"))

(defmethod list-problem-inputs ((api api) pid)
  (let ((url (format nil "~a/~a/~a" (api-base-url api) "UVa" pid)))
    (multiple-value-bind
	  (stream status-code headers uri stream2 must-close reason-phrase)
	(drakma:http-request
	 url
	 :method :get
	 :verify :required
	 :proxy (api-proxy-server api)
	 :proxy-basic-authorization (api-proxy-auth api)
	 :connection-timeout (api-timeout api)
	 :cookie-jar (api-cookie-jar api)
	 :want-stream t
	 :close t)
      (declare (ignore headers uri stream2 reason-phrase))
      (if (= status-code 200)
	  (let* ((html (plump:parse stream))
		 (div-input-table (if html
				      (clss:select "div.select_input_table" html)
				      nil))
		 (input-table-rows (if div-input-table
				       (clss:select "table tbody tr" div-input-table)
				       nil)))
	    (when must-close
	      (close stream))
	    (loop :for single-row :across input-table-rows
		  :collect
		  (let* ((columns (clss:select "td" single-row))
			 (id (nth-value 0 (parse-integer (plump:text (aref columns 0)))))
			 (user (plump:text (aref columns 1)))
			 (date (plump:text (aref columns 2)))
			 (votes (nth-value 0 (parse-integer (plump:text (aref columns 3))))))
		    `((:id . ,id) (:user . ,user) (:date . ,date) (:votes . ,votes)))))
	  (error 'simple-error :format-control "~A: URL request error" :format-arguments (list status-code))))))

(defmethod get-problem-input ((api api) pid input-id)
  (let ((url (format nil "~a/~a/~a" (api-base-url api) "UVa" pid)))
    (multiple-value-bind
	  (stream status-code headers uri stream2 must-close reason-phrase)
	(drakma:http-request
	 url
	 :method :get
	 :verify :required
	 :proxy (api-proxy-server api)
	 :proxy-basic-authorization (api-proxy-auth api)
	 :connection-timeout (api-timeout api)
	 :cookie-jar (api-cookie-jar api)
	 :want-stream t
	 :close t)
      (declare (ignore headers uri stream2 reason-phrase))
      (if (= status-code 200)
	  (let* ((html (plump:parse stream))
		 (div-input-table (if html
				      (clss:select "div.select_input_table" html)
				      nil))
		 (input-table-rows (if div-input-table
				       (clss:select "table tbody tr" div-input-table)
				       nil)))
	    (when must-close
	      (close stream))
	    (loop :for single-row :across input-table-rows
		  :when (= input-id (nth-value 0 (parse-integer (plump:text (aref (clss:select "td" single-row) 0)))))
		    :return
		    (get-input-data api
				    (nth-value 0 (parse-integer (plump:attribute (aref (clss:select "a.input_desc" (clss:select "td" single-row)) 1) "data-id"))))))
	  (error 'simple-error :format-control "~A: URL request error" :format-arguments (list status-code))))))

(defmethod get-input-data ((api api) data-id)
  (let ((url (format nil "~a/~a" (api-base-url api) "udebug-custom-get-selected-input-ajax")))
    (multiple-value-bind
	  (stream status-code headers uri stream2 must-close reason-phrase)
	(drakma:http-request
	 url
	 :method :post
	 :parameters `(("input_nid" . ,(format nil "~d" data-id)))
	 :verify :required
	 :proxy (api-proxy-server api)
	 :proxy-basic-authorization (api-proxy-auth api)
	 :connection-timeout (api-timeout api)
	 :cookie-jar (api-cookie-jar api)
	 :want-stream t
	 :close t)
      (declare (ignore headers uri stream2 reason-phrase))
      (if (= status-code 200)
	  (let ((result (json:decode-json stream)))
	    (when must-close
	      (close stream))
	    (cdr (assoc :input--value result)))
	  (error 'simple-error :format-control "~A: URL request error" :format-arguments (list status-code))))))

(defmethod get-accepted-output ((api api) pid input-data)
  (let ((url (format nil "~a/~a/~a" (api-base-url api) "UVa" pid)))
    (multiple-value-bind
	  (stream status-code headers uri stream2 must-close reason-phrase)
	(drakma:http-request
	 url
	 :method :get
	 :verify :required
	 :proxy (api-proxy-server api)
	 :proxy-basic-authorization (api-proxy-auth api)
	 :connection-timeout (api-timeout api)
	 :cookie-jar (api-cookie-jar api)
	 :want-stream t
	 :close t)
      (declare (ignore headers uri stream2 reason-phrase))
      (if (= status-code 200)
	  (let* ((html (plump:parse stream))
		 (form (clss:select "#udebug-custom-problem-view-input-output-form" html))
		 (hidden-inputs (loop :for input :across (clss:select "input" form)
				      :when (string= "hidden" (plump:attribute input "type"))
					:collect
					(cons (plump:attribute input "name") (plump:attribute input "value")))))
	    (when must-close
	      (close stream))
	    (request-output-data
	     api
	     pid
	     (or (cdr (assoc "problem_nid" hidden-inputs :test #'string=)) "")
	     (or (cdr (assoc "node_nid" hidden-inputs :test #'string=)) "")
	     (or (cdr (assoc "form_build_id" hidden-inputs :test #'string=)) "")
	     (or (cdr (assoc "form_id" hidden-inputs :test #'string=)) "")
	     input-data))
	  (error 'simple-error :format-control "~A: URL request error" :format-arguments (list status-code))))))

;;;; For some reason, plump can not return the text of a text area,
;;;; or I am too stupid to do it, so here is a workaround.
;;;; TODO: FIXME
(defun textarea-text (textarea)
  (let ((stream (make-string-output-stream)))
    (plump:serialize textarea stream)
    (plump:text (plump:parse (cl-ppcre:regex-replace-all "textarea>$"
							 (cl-ppcre:regex-replace-all "^<textarea"
										     (get-output-stream-string stream)
										     "<span")
							 "span>")))))

(defmethod request-output-data ((api api) pid problem-nid node-nid form-build-id form-id input-data)
  (let ((url (format nil "~a/~a/~a" (api-base-url api) "UVa" pid)))
    (multiple-value-bind
	  (stream status-code headers uri stream2 must-close reason-phrase)
	(drakma:http-request
	 url
	 :method :post
	 :parameters `(("problem_nid" . ,problem-nid)
		       ("input_data" . ,input-data)
		       ("node_nid" . ,node-nid)
		       ("op" . "Get Accepted Output")
		       ("user_output" . "")
		       ("form_build_id" . ,form-build-id)
		       ("form_id" . ,form-id))
	 :verify :required
	 :proxy (api-proxy-server api)
	 :proxy-basic-authorization (api-proxy-auth api)
	 :connection-timeout (api-timeout api)
	 :cookie-jar (api-cookie-jar api)
	 :want-stream t
	 :close t)
      (declare (ignore headers uri stream2 reason-phrase))
      (if (= status-code 200)
	  (let* ((html (plump:parse stream))
		 (output (clss:select "#edit-output-data" html)))
	    (when must-close
	      (close stream))
	    (textarea-text output))
	  (error 'simple-error :format-control "~A: URL request error" :format-arguments (list status-code))))))
