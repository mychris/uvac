;;;; ojudge.lisp

(defpackage #:ojudge
  (:use #:common-lisp #:cl)
  (:export
   #:api
   #:login
   #:submit
   ))
(in-package #:ojudge)

(defclass api ()
  ((base-url :accessor api-base-url
	     :initarg :base-url
	     :documentation
	     "Base url for the uhunt online API")
   (timeout :accessor api-timeout
	    :initarg :timeout
	    :documentation
	    "The time (in seconds) until an attempt to connect to a server is considered a failure")
   (proxy-server :accessor api-proxy-server
		 :initarg :proxy-server
		 :documentation
		 "A list of two values - a string denoting the proxy server and an integer denoting the port to use")
   (proxy-auth :accessor api-proxy-auth
	       :initarg :proxy-auth
	       :documentation
	       "A list of two strings (username and password) which will be sent to the proxy server for authorization")
   (cookie-jar :accessor api-cookie-jar
	       :initarg :cookie-jar
	       :documentation
	       "A cookie jar containing cookies which will potentially be sent to the server")
   )
  (:default-initargs
   :base-url "https://onlinejudge.org/"
   :timeout 5
   :proxy-server nil
   :proxy-auth nil
   :cookie-jar (make-instance 'drakma:cookie-jar)
   )
  (:documentation "Make requests to the uhunt online API"))


(defmethod request ((api api) &optional (path "") (method :get) (parameters nil))
  (let ((url (format nil "~a~a" (api-base-url api) path)))
    (drakma:http-request url
			 :method method
			 :parameters parameters
			 :verify :required
			 :proxy (api-proxy-server api)
			 :proxy-basic-authorization (api-proxy-auth api)
			 :connection-timeout (api-timeout api)
			 :cookie-jar (api-cookie-jar api)
			 :want-stream t
			 :close t)))

(defmethod login ((api api) username password)
  (multiple-value-bind
	(stream status-code headers uri stream2 must-close reason-phrase)
      (request api)
    (declare (ignore headers uri stream2))
    (if (= status-code 200)
	(let* ((html (plump:parse stream))
	       (form (clss:select "form#mod_loginform" html))
	       (hidden-inputs (loop :for input :across (clss:select "input" form)
				    :when (string= "hidden" (plump:attribute input "type"))
				      :collect
				      (cons (plump:attribute input "name") (plump:attribute input "value"))))
	       (submit-inputs (loop :for input :across (clss:select "input" form)
				    :when (string= "submit" (plump:attribute input "type"))
				      :collect
				      (cons (plump:attribute input "name") (plump:attribute input "value")))))
	  (when must-close
	    (close stream))
	  (multiple-value-bind
		(stream status-code headers uri stream2 must-close reason-phrase)
	      (request api "index.php?option=com_comprofiler&task=login" :post (append `(("username" . ,username)
											 ("passwd" . ,password))
										       hidden-inputs
										       submit-inputs))
	    (declare (ignore headers uri stream2))
	    (if (= status-code 200)
		(let* ((html (plump:parse stream))
		       (left-column (clss:select "div#col1_content" html))
		       (links-in-left-column (clss:select "a" left-column)))
		  (when must-close
		    (close stream))
		  (when (not (loop :for link :across links-in-left-column
				   :when (string= "Logout" (plump:text link))
				     :return t
				   :finally (return nil)))
		    (login-error html))
		  )
		(error 'simple-error :format-control "Error ~A: ~A" :format-arguments (list status-code reason-phrase))))
	  )
	(error 'simple-error :format-control "Error ~A: ~A" :format-arguments (list status-code reason-phrase)))))

(defmethod submit ((api api) problem-id language code)
  (multiple-value-bind
	(stream status-code headers uri stream2 must-close reason-phrase)
      (request api "index.php?option=com_onlinejudge&Itemid=25")
    (declare (ignore headers uri stream2))
    (if (= status-code 200)
	(let* ((html (plump:parse stream))
	       (form (find-submit-form-on-page html))
	       (hidden-inputs (loop :for input :across (clss:select "input" form)
				    :when (string= "hidden" (plump:attribute input "type"))
				      :collect
				      (cons (plump:attribute input "name") (plump:attribute input "value"))))
	       (submit-inputs (loop :for input :across (clss:select "input" form)
				    :when (string= "submit" (plump:attribute input "type"))
				      :collect
				      (cons (plump:attribute input "name") (plump:attribute input "value")))))
	  (when must-close
	    (close stream))
	  (format t "~A~%" (plump:attribute form "action"))
	  (loop :for input :across (clss:select "input" form) :do
	    (format t "~a~%" (plump:serialize input nil)))
	  (multiple-value-bind
		(stream status-code headers uri stream2 must-close reason-phrase)
	      (request api (plump:attribute form "action") :post (append `(("localid" . ,problem-id)
									   ("language" . ,language)
									   ("code" . ,code))
									 hidden-inputs
									 submit-inputs))
	    (declare (ignore headers uri stream2))
	    (if (= status-code 200)
		(let* ((html (plump:parse stream)))
		  (when must-close
		    (close stream))
		  (format t "~A~%" html))
		(error 'simple-error :format-control "Error ~A: ~A" :format-arguments (list status-code reason-phrase)))))
	(error 'simple-error :format-control "Error ~A: ~A" :format-arguments (list status-code reason-phrase)))))

(defun find-submit-form-on-page (html)
  (let ((form (clss:select "div#col3 form" html)))
    (if (/= 1 (length form))
	(error 'simple-error :format-control "Can not find login form")
	(aref form 0))))

(defun login-error (html)
  (let ((err (clss:select "div#col3_content div.error" html)))
    (if (/= 0 (length err))
	(error 'simple-error :format-control "Failed to login: ~A" :format-arguments (list (plump:text (aref err 0))))
	(error 'simple-error :format-control "Failed to login"))))
