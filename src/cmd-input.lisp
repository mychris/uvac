;;;; cmd-input.lisp

(defpackage #:cmd-input
  (:use #:common-lisp #:cl)
  (:export
   #:make-command))
(in-package #:cmd-input)

(defun make-tableizer-for-input-list ()
  (utils:make-tableizer
   (list (utils:make-tableizer-column
	  :name "id"
	  :header "id"
	  :data-extractor (lambda (x) (cdr (assoc :id x))))
	 (utils:make-tableizer-column
	  :name "user"
	  :header "user"
	  :data-extractor (lambda (x) (cdr (assoc :user x))))
	 (utils:make-tableizer-column
	  :name "date"
	  :header "date"
	  :data-extractor (lambda (x) (cdr (assoc :date x))))
	 (utils:make-tableizer-column
	  :name "votes"
	  :header "votes"
	  :data-extractor (lambda (x) (cdr (assoc :votes x)))))))

(defun retrieve-input-list (pid)
  (let* ((api (make-instance 'udebug:api))
	 (input-list (udebug:list-problem-inputs api pid))
	 (tbl (make-tableizer-for-input-list)))
    (utils:tableizer-format tbl uiop:*stdout* input-list)))

(defun retrieve-input (pid iid)
  (let* ((api (make-instance 'udebug:api))
	 (input (udebug:get-problem-input api pid iid)))
    (if (not input)
	(utils:err "Invalid problem and/or input IDs: ~A ~A~%" pid iid)
	(format t "~A~&" input))))

(defun command-handler (cmd)
  (when (not (clingon:command-arguments cmd))
    (clingon:print-usage-and-exit cmd *error-output*))
  (let ((pid (first (clingon:command-arguments cmd)))
	(iid (second (clingon:command-arguments cmd))))
    (handler-case
	(setf pid (nth-value 0 (parse-integer pid)))
      (error () (utils:err "Invalid problem ID: ~A" pid)))
    (handler-case
	(when iid (setf iid (nth-value 0 (parse-integer iid))))
      (error () (utils:err "Invalid input ID: ~A" iid)))
    (if iid
	(retrieve-input pid iid)
	(retrieve-input-list pid))))

(defun make-options ()
  (list
   ))

(defun make-command ()
  (clingon:make-command
   :name "input"
   :description "Retrieve problem input from uDebug."
   :options (make-options)
   :usage "[options] problem [input]"
   :long-description nil
   :handler #'command-handler))
