;;;; cmd-rank.lisp

(defpackage #:cmd-rank
  (:use #:common-lisp #:cl)
  (:export
   #:make-command))
(in-package #:cmd-rank)

(defun command-handler (cmd)
  (when (not (clingon:command-arguments cmd))
    (clingon:print-usage-and-exit cmd *error-output*))
  (let* ((api (make-instance 'uhunt:api))
	 (username (first (clingon:command-arguments cmd)))
	 (uid (uhunt:uname-to-uid api username)))
    (if (= 0 uid)
	(utils:err "Unknown username: ~A" username)
	(let ((rank (uhunt:ranklist api uid 0 0)))
	  (format uiop:*stdout* "~d~%" (uhunt:uva-user-rank-rank (first rank)))))))

(defun make-options ()
  (list
   ))

(defun make-command ()
  (clingon:make-command
   :name "rank"
   :description "Show the UVa rank from uhunt of the given user."
   :options (make-options)
   :usage "[options] username"
   :long-description nil
   :handler #'command-handler))
