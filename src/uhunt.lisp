;;;; uhunt.lisp

(defpackage #:uhunt
  (:use #:common-lisp #:cl)
  (:export
   #:+submission-error+
   #:+can-not-be-judged+
   #:+in-queue+
   #:+compile-error+
   #:+restricted-function+
   #:+runtime-error+
   #:+output-limit+
   #:+time-limit+
   #:+memory-limit+
   #:+wrong-answer+
   #:+presentation-error+
   #:+accepted+
   #:+lang-ansi-c+
   #:+lang-java+
   #:+lang-c+++
   #:+lang-pascal+
   #:+lang-c++11+
   #:uva-problem
   #:uva-problem-pid
   #:uva-problem-num
   #:uva-problem-title
   #:uva-problem-dacu
   #:uva-problem-mrun
   #:uva-problem-mmem
   #:uva-problem-nover
   #:uva-problem-sube
   #:uva-problem-noj
   #:uva-problem-inq
   #:uva-problem-ce
   #:uva-problem-rf
   #:uva-problem-re
   #:uva-problem-ole
   #:uva-problem-tle
   #:uva-problem-mle
   #:uva-problem-wa
   #:uva-problem-pe
   #:uva-problem-ac
   #:uva-problem-rtl
   #:uva-problem-status
   #:uva-problem-rej
   #:uva-submission
   #:uva-submission-sid
   #:uva-submission-pid
   #:uva-submission-verdict
   #:uva-submission-runtime
   #:uva-submission-submission-time
   #:uva-submission-language
   #:uva-submission-submission-rank
   #:uva-user-rank
   #:uva-user-rank-uid
   #:uva-user-rank-rank
   #:uva-user-rank-name
   #:uva-user-rank-username
   #:uva-user-rank-ac
   #:uva-user-rank-nos
   #:api
   #:all-problems
   #:problem-by-pid
   #:problem-by-num
   #:uname-to-uid
   #:solved-problems
   #:submissions
   #:submissions-last
   #:ranklist
   #:rank
   ))
(in-package #:uhunt)

(defconstant +submission-error+ 10)
(defconstant +can-not-be-judged+ 15)
(defconstant +in-queue+ 20)
(defconstant +compile-error+ 30)
(defconstant +restricted-function+ 35)
(defconstant +runtime-error+ 40)
(defconstant +output-limit+ 45)
(defconstant +time-limit+ 50)
(defconstant +memory-limit+ 60)
(defconstant +wrong-answer+ 70)
(defconstant +presentation-error+ 80)
(defconstant +accepted+ 90)

(defconstant +lang-ansi-c+ 1)
(defconstant +lang-java+ 2)
(defconstant +lang-c+++ 3)
(defconstant +lang-pascal+ 4)
(defconstant +lang-c++11+ 5)

(defclass uva-problem ()
  ((pid :accessor uva-problem-pid
	:initarg :pid
	:documentation "Problem ID")
   (num :accessor uva-problem-num
	:initarg :num
	:documentation "Problem Number")
   (title :accessor uva-problem-title
	  :initarg :title
	  :documentation "Problem Title")
   (dacu :accessor uva-problem-dacu
	 :initarg :dacu
	 :documentation "Number of Distinct Accepted User (DACU)")
   (mrun :accessor uva-problem-mrun
	 :initarg :mrun
	 :documentation "Best Runtime of an Accepted Submission")
   (mmem :accessor uva-problem-mmem
	 :initarg :mmem
	 :documentation "Best Memory used of an Accepted Submission")
   (nover :accessor uva-problem-nover
	  :initarg :nover
	  :documentation "Number of No Verdict Given (can be ignored)")
   (sube :accessor uva-problem-sube
	 :initarg :sube
	 :documentation "Number of Submission Error")
   (noj :accessor uva-problem-noj
	:initarg :noj
	:documentation "Number of Can't be Judged")
   (inq :accessor uva-problem-inq
	:initarg :inq
	:documentation "Number of In Queue")
   (ce :accessor uva-problem-ce
       :initarg :ce
       :documentation "Number of Compilation Error")
   (rf :accessor uva-problem-rf
       :initarg :rf
       :documentation "Number of Restricted Function")
   (re :accessor uva-problem-re
       :initarg :re
       :documentation "Number of Runtime Error")
   (ole :accessor uva-problem-ole
	:initarg :ole
	:documentation "Number of Output Limit Exceeded")
   (tle :accessor uva-problem-tle
	:initarg :tle
	:documentation "Number of Time Limit Exceeded")
   (mle :accessor uva-problem-mle
	:initarg :mle
	:documentation "Number of Memory Limit Exceeded")
   (wa :accessor uva-problem-wa
       :initarg :wa
       :documentation "Number of Wrong Answer")
   (pe :accessor uva-problem-pe
       :initarg :pe
       :documentation "Number of Presentation Error")
   (ac :accessor uva-problem-ac
       :initarg :ac
       :documentation "Number of Accepted")
   (rtl :accessor uva-problem-rtl
	:initarg :rtl
	:documentation "Problem Run-Time Limit (milliseconds)")
   (status :accessor uva-problem-status
	   :initarg :status
	   :documentation "Problem Status (0 = unavailable, 1 = normal, 2 = special judge)")
   (rej :accessor uva-problem-rej
	:initarg :rej
	:documentation "Undocumented"))
  (:documentation
   "A single UVa problem"))

(defun make-uva-problem-from-list (lst)
  (make-instance 'uva-problem
		 :pid (nth 0 lst)
		 :num (nth 1 lst)
		 :title (nth 2 lst)
		 :dacu (nth 3 lst)
		 :mrun (nth 4 lst)
		 :mmem (nth 5 lst)
		 :nover (nth 6 lst)
		 :sube (nth 7 lst)
		 :noj (nth 8 lst)
		 :inq (nth 9 lst)
		 :ce (nth 10 lst)
		 :rf (nth 11 lst)
		 :re (nth 12 lst)
		 :ole (nth 13 lst)
		 :tle (nth 14 lst)
		 :mle (nth 15 lst)
		 :wa (nth 16 lst)
		 :pe (nth 17 lst)
		 :ac (nth 18 lst)
		 :rtl (nth 19 lst)
		 :status (nth 20 lst)
		 :rej (nth 21 lst)))

(defun make-uva-problem-from-alist (alst)
  (make-instance 'uva-problem
		 :pid (cdr (assoc :pid alst))
		 :num (cdr (assoc :num alst))
		 :title (cdr (assoc :title alst))
		 :dacu (cdr (assoc :dacu alst))
		 :mrun (cdr (assoc :mrun alst))
		 :mmem (cdr (assoc :mmem alst))
		 :nover (cdr (assoc :nover alst))
		 :sube (cdr (assoc :sube alst))
		 :noj (cdr (assoc :noj alst))
		 :inq (cdr (assoc :inq alst))
		 :ce (cdr (assoc :ce alst))
		 :rf (cdr (assoc :rf alst))
		 :re (cdr (assoc :re alst))
		 :ole (cdr (assoc :ole alst))
		 :tle (cdr (assoc :tle alst))
		 :mle (cdr (assoc :mle alst))
		 :wa (cdr (assoc :wa alst))
		 :pe (cdr (assoc :pe alst))
		 :ac (cdr (assoc :ac alst))
		 :rtl (cdr (assoc :rtl alst))
		 :status (cdr (assoc :status alst))
		 :rej (cdr (assoc :rej alst))))

(defclass uva-submission ()
  ((sid :accessor uva-submission-sid
	:initarg :sid
	:type integer
	:documentation "Submission ID")
   (pid :accessor uva-submission-pid
	:initarg :pid
	:type integer
	:documentation "Problem ID")
   (verdict :accessor uva-submission-verdict
	    :initarg :verdict
	    :type integer
	    :documentation "Verdict ID")
   (runtime :accessor uva-submission-runtime
	    :initarg :runtime
	    :type integer
	    :documentation "Runtime")
   (submission-time :accessor uva-submission-submission-time
		    :initarg :submission-time
		    :type integer
		    :documentation "Submission Time (unix timestamp)")
   (language :accessor uva-submission-language
	     :initarg :language
	     :type integer
	     :documentation "Language ID")
   (submission-rank :accessor uva-submission-submission-rank
		    :initarg :submission-rank
		    :type integer
		    :documentation "Submission Rank"))
  (:documentation
   "A single submission"))

(defmethod print-object ((obj uva-submission) stream)
  (print-unreadable-object (obj stream :type t)
    (format
     stream
     ":sid ~a :pid ~a :verdict ~a :runtime ~a :submission-time ~a :language ~a :submission-rank ~a"
     (uva-submission-sid obj)
     (uva-submission-pid obj)
     (uva-submission-verdict obj)
     (uva-submission-runtime obj)
     (uva-submission-submission-time obj)
     (uva-submission-language obj)
     (uva-submission-submission-rank obj)
     )))

(defun make-uva-submission-from-list (lst)
  (make-instance 'uva-submission
		 :sid (nth 0 lst)
		 :pid (nth 1 lst)
		 :verdict (nth 2 lst)
		 :runtime (nth 3 lst)
		 :submission-time (nth 4 lst)
		 :language (nth 5 lst)
		 :submission-rank (nth 6 lst)))

(defclass uva-user-rank ()
  ((uid :accessor uva-user-rank-uid
	:initarg :uid
	:type integer
	:documentation "ID of the user")
   (rank :accessor uva-user-rank-rank
	 :initarg :rank
	 :type integer
	 :documentation "Rank of the user")
   (name :accessor uva-user-rank-name
	 :initarg :name
	 :type string
	 :documentation "Name of the user")
   (username :accessor uva-user-rank-username
	     :initarg :username
	     :type string
	     :documentation "Username of the user")
   (ac :accessor uva-user-rank-ac
       :initarg :ac
       :type integer
       :documentation "The number of accepted problems")
   (nos :accessor uva-user-rank-nos
	:initarg :nos
	:type integer
	:documentation "The number of submissions of the user"))
  (:documentation
   "The rank of a user in the World Ranklist"))

(defmethod print-object ((obj uva-user-rank) stream)
  (print-unreadable-object (obj stream :type t)
    (format
     stream
     ":uid ~a :rank ~a :name ~a :username ~a :ac ~a :nos ~a"
     (uva-user-rank-uid obj)
     (uva-user-rank-rank obj)
     (uva-user-rank-name obj)
     (uva-user-rank-username obj)
     (uva-user-rank-ac obj)
     (uva-user-rank-nos obj)
     )))

(defun make-uva-user-rank-from-alist (alst)
  (make-instance 'uva-user-rank
		 :uid (cdr (assoc :userid alst))
		 :rank (cdr (assoc :rank alst))
		 :name (cdr (assoc :name alst))
		 :username (cdr (assoc :username alst))
		 :ac (cdr (assoc :ac alst))
		 :nos (cdr (assoc :nos alst))))

(defclass api ()
  ((base-url :accessor api-base-url
	     :initarg :base-url
	     :documentation
	     "Base url for the uhunt online API")
   (timeout :accessor api-timeout
	    :initarg :timeout
	    :documentation
	    "The time (in seconds) until an attempt to connect to a server is considered a failure")
   (proxy-server :accessor api-proxy-server
		 :initarg :proxy-server
		 :documentation
		 "A list of two values - a string denoting the proxy server and an integer denoting the port to use")
   (proxy-auth :accessor api-proxy-auth
	       :initarg :proxy-auth
	       :documentation
	       "A list of two strings (username and password) which will be sent to the proxy server for authorization")
   (cookie-jar :accessor api-cookie-jar
	       :initarg :cookie-jar
	       :documentation
	       "A cookie jar containing cookies which will potentially be sent to the server")
   )
  (:default-initargs
   :base-url "https://uhunt.onlinejudge.org/api"
   :timeout 5
   :proxy-server nil
   :proxy-auth nil
   :cookie-jar (make-instance 'drakma:cookie-jar)
   )
  (:documentation "Make requests to the uhunt online API"))

(defmethod make-request ((api api) endpoint method &rest args)
  (let ((url (format nil "~{~A~^/~}" (append (list (api-base-url api) endpoint) args))))
    (multiple-value-bind
	  (stream status-code headers uri stream2 must-close reason-phrase)
	(drakma:http-request
	 url
	 :method method
	 :verify :required
	 :proxy (api-proxy-server api)
	 :proxy-basic-authorization (api-proxy-auth api)
	 :connection-timeout (api-timeout api)
	 :cookie-jar (api-cookie-jar api)
	 :want-stream t
	 :close t)
      (declare (ignore headers uri stream2 reason-phrase))
      (if (= status-code 200)
	  (let ((result (json:decode-json stream)))
	    (when must-close
	      (close stream))
	    result)
	  (error 'simple-error :format-control "~A: URL request error" :format-arguments (list status-code))))))

(defmethod all-problems ((api api))
  (mapcar
   #'make-uva-problem-from-list
   (make-request api "p" :get)))

(defmethod problem-by-pid ((api api) pid)
  (make-uva-problem-from-alist (make-request api "p/id" :get pid)))

(defmethod problem-by-num ((api api) num)
  (make-uva-problem-from-alist (make-request api "p/num" :get num)))

(defmethod uname-to-uid ((api api) uname)
  (make-request api "uname2uid" :get uname))

(defun extract-bits (num pos)
  (loop :for i :from 0 :below 32
	:when (/= (logand (ash 1 i) num) 0)
	  :collect (+ (* pos 32) i)))

(defun bits-to-list (bitlist)
  (let ((result nil))
    (loop :for bits :in bitlist
	  :for pos :from 0
	  :do (setf result (append result (extract-bits bits pos))))
    result))

(defmethod solved-problems ((api api) &rest user-ids)
  (let ((json-result (make-request api "solved-bits" :get (format nil "~{~A~^,~}" user-ids))))
    (loop :for user-result :in json-result
	  :collect (cons (cdr (assoc :uid user-result))
			 (bits-to-list (cdr (assoc :solved user-result)))))))

(defmethod submissions ((api api) uid &optional min-sid)
  (let ((json-result (if min-sid
			 (make-request api "subs-user" :get uid min-sid)
			 (make-request api "subs-user" :get uid))))
    (loop :for submission-list :in (cdr (assoc :subs json-result))
	  :collect (make-uva-submission-from-list submission-list))))

(defmethod submissions-last ((api api) uid count)
  (let ((json-result (make-request api "subs-user-last" :get uid count)))
    (loop :for submission-list :in (cdr (assoc :subs json-result))
	  :collect (make-uva-submission-from-list submission-list))))

(defmethod ranklist ((api api) uid nabove nbelow)
  (mapcar
   #'make-uva-user-rank-from-alist
   (make-request api "ranklist" :get uid nabove nbelow)))

(defmethod rank ((api api) pos count)
  (mapcar
   #'make-uva-user-rank-from-alist
   (make-request api "rank" :get pos count)))
