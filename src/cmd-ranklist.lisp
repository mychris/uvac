;;;; cmd-ranklist.lisp

(defpackage #:cmd-ranklist
  (:use #:common-lisp #:cl)
  (:export
   #:make-command))
(in-package #:cmd-ranklist)

(defun make-tableizer-for-ranklist (cmd)
  (let ((outputs (ppcre:split "," (if (clingon:opt-is-set-p cmd :output-all)
				      "RANK,NAME,USERNAME,AC,NOS"
				      (string-upcase (clingon:getopt cmd :output))))))
    (make-instance
     'utils:tableizer
     :columns
     (loop :for output :in outputs :collect
				   (cond
				     ((string= "RANK" output)
				      (make-instance 'utils:tableizer-column
						     :name output
						     :header (string-downcase output)
						     :data-extractor #'uhunt:uva-user-rank-rank))
				     ((string= "NAME" output)
				      (make-instance 'utils:tableizer-column
						     :name output
						     :header (string-downcase output)
						     :data-extractor #'uhunt:uva-user-rank-name))
				     ((string= "USERNAME" output)
				      (make-instance 'utils:tableizer-column
						     :name output
						     :header (string-downcase output)
						     :data-extractor #'uhunt:uva-user-rank-username))
				     ((string= "AC" output)
				      (make-instance 'utils:tableizer-column
						     :name output
						     :header (string-downcase output)
						     :data-extractor #'uhunt:uva-user-rank-ac))
				     ((string= "NOS" output)
				      (make-instance 'utils:tableizer-column
						     :name output
						     :header (string-downcase output)
						     :data-extractor #'uhunt:uva-user-rank-nos)))))))

(defun ranklist-for-user (username cmd)
  (let* ((above (clingon:getopt cmd :above))
	 (below (clingon:getopt cmd :below))
	 (api (make-instance 'uhunt:api))
	 (uid (uhunt:uname-to-uid api username)))
    (if (= 0 uid)
	(utils:err "Unknown username: ~A" username)
	(let ((ranklist (uhunt:ranklist api uid above below)))
	  (utils:tableizer-format (make-tableizer-for-ranklist cmd) uiop:*stdout* ranklist :show-headings (not (clingon:getopt cmd :noheadings)))))))

(defun ranklist-for-position (position cmd)
  (let* ((above (clingon:getopt cmd :above))
	 (below (clingon:getopt cmd :below))
	 (pos (- position above))
	 (count (+ 1 above below))
	 (api (make-instance 'uhunt:api)))
    (let ((ranklist (uhunt:rank api pos count)))
      (utils:tableizer-format (make-tableizer-for-ranklist cmd) uiop:*stdout* ranklist :show-headings (not (clingon:getopt cmd :noheadings))))))

(defun command-handler (cmd)
  (when (not (clingon:command-arguments cmd))
    (clingon:print-usage-and-exit cmd *error-output*))
  (loop :for name-or-pos :in (clingon:command-arguments cmd) :do
    (if (ppcre:scan "^[1-9][0-9]*$" name-or-pos)
	(ranklist-for-position (nth-value 0 (parse-integer name-or-pos)) cmd)
	(ranklist-for-user name-or-pos cmd))))

(defun make-options ()
  (list
   (clingon:make-option
    :flag
    :description "output all columns"
    :short-name #\O
    :long-name "output-all"
    :initial-value nil
    :key :output-all)
   (clingon:make-option
    :string
    :description "output columns (comma separated list)"
    :short-name #\o
    :long-name "output"
    :initial-value "rank,username"
    :key :output)
   (clingon:make-option
    :flag
    :description "do not print headings"
    :short-name #\n
    :long-name "noheadings"
    :initial-value nil
    :key :noheadings)
   (clingon:make-option
    :integer
    :description "number of ranks above"
    :short-name #\a
    :long-name "above"
    :initial-value 10
    :key :above)
   (clingon:make-option
    :integer
    :description "number of ranks below"
    :short-name #\b
    :long-name "below"
    :initial-value 10
    :key :below)
   ))

(defun make-command ()
  (clingon:make-command
   :name "ranklist"
   :description "Show the UVa ranklist from uhunt."
   :options (make-options)
   :usage "[options] username|position"
   :long-description "Displays the UVa ranklist table, showing either a certain position,
or a certain user.

Available output columns:
      RANK  The rank of the user
      NAME  The name of the user
  USERNAME  The username of the user
        AC  The number of accepted problems
       NOS  The number of submissions of the user"
   :handler #'command-handler))
