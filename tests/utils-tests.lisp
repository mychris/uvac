(in-package :uvac-tests)

(def-suite utils-unit-tests
  :description "Unit tests for the utils package"
  :in unit-tests)

(in-suite utils-unit-tests)

(test test-tableizer-empty-data
      (let ((sstream (make-string-output-stream))
	    (tableizer (make-instance 'utils:tableizer
				      :columns nil)))
	(utils:tableizer-format tableizer sstream nil)
	(is (string= "" (get-output-stream-string sstream)))))


(test test-tableizer-no-usable-data
      (let ((sstream (make-string-output-stream))
	    (tableizer (utils:make-tableizer (list
					      (utils:make-tableizer-column
					       :name "rank"
					       :header "rank"
					       :data-extractor (lambda (x) (cdr (assoc :rank x))))))))
	(utils:tableizer-format tableizer sstream '(((:username . "first user")) ((:username . "second user"))))
	(is (string= (format nil "rank~%    ~%    ~%") (get-output-stream-string sstream)))))

(test test-tableizer-single-column
      (let ((sstream (make-string-output-stream))
	    (tableizer (utils:make-tableizer (list
					      (utils:make-tableizer-column
					       :name "username"
					       :header "username"
					       :data-extractor (lambda (x) (cdr (assoc :username x))))))))
	(utils:tableizer-format tableizer sstream '(((:username . "first user")) ((:username . "second user"))))
	(is (string= (format nil "   username~%first user ~%second user~%") (get-output-stream-string sstream)))))

(test test-tableizer-two-columns
      (let ((sstream (make-string-output-stream))
	    (tableizer (utils:make-tableizer (list
					      (utils:make-tableizer-column
					       :name "username"
					       :header "username"
					       :data-extractor (lambda (x) (cdr (assoc :username x))))
					      (utils:make-tableizer-column
					       :name "rank"
					       :header "rank"
					       :data-extractor (lambda (x) (cdr (assoc :rank x))))))))
	(utils:tableizer-format tableizer sstream '(((:rank . 5) (:username . "first user")) ((:username . "second user") (:rank . 7))))
	(is (string= (format nil "   username rank~%first user  5   ~%second user 7   ~%") (get-output-stream-string sstream)))))
