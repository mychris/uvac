(in-package :uvac-tests)

(in-suite integration-tests)

(test test-uname-to-uid
      (let ((api (make-instance 'uhunt:api)))
	(is (= 76783 (uhunt:uname-to-uid api "mychris")))
	(is (= 339 (uhunt:uname-to-uid api "felix_halim")))))

(test test-uhunt-problem-by-pid
      (let* ((api (make-instance 'uhunt:api))
	     (actual (uhunt:problem-by-pid api 36)))
	(is (= 100 (uhunt:uva-problem-num actual)))
	(is (string= "The 3n + 1 problem" (uhunt:uva-problem-title actual)))))

(test test-uhunt-problem-by-num
      (let* ((api (make-instance 'uhunt:api))
	     (actual (uhunt:problem-by-num api 100)))
	(is (= 36 (uhunt:uva-problem-pid actual)))
	(is (string= "The 3n + 1 problem" (uhunt:uva-problem-title actual)))))

(test test-uhunt-ranklist
      (let* ((api (make-instance 'uhunt:api))
	     (actual-all (uhunt:ranklist api 76783 1 1))
	     (actual-below (first actual-all))
	     (actual-me (second actual-all))
	     (actual-above (third actual-all)))
	(is (string= "mychris" (uhunt:uva-user-rank-username actual-me)))
	(is (<= 359 (uhunt:uva-user-rank-ac actual-me)))
	(is (<= 775 (uhunt:uva-user-rank-nos actual-me)))
	(is (>= (uhunt:uva-user-rank-ac actual-me) (uhunt:uva-user-rank-ac actual-above)))
	(is (>= (uhunt:uva-user-rank-ac actual-below) (uhunt:uva-user-rank-ac actual-me)))
	(is (< (uhunt:uva-user-rank-rank actual-me) (uhunt:uva-user-rank-rank actual-above)))
	(is (< (uhunt:uva-user-rank-rank actual-below) (uhunt:uva-user-rank-rank actual-me)))))
