(in-package :uvac)
(defpackage #:uvac-tests
  (:use #:common-lisp #:cl #:fiveam)
  (:export #:run!
	   #:all-tests
	   #:unit-tests
	   #:utils-unit-tests
	   #:integration-tests))
(in-package :uvac-tests)

(def-suite all-tests
  :description "All tests for the UVa cli utility")

(def-suite unit-tests
  :description "All unit tests for the UVa cli utility"
  :in all-tests)

(def-suite integration-tests
  :description "All integration tests for the UVa cli utility"
  :in all-tests)
