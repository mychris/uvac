;;;; uvac.asd

(defsystem uvac
  :author "Christoph Göttschkes"
  :maintainer "Christoph Göttschkes"
  :license "ISC"
  :homepage "https://codeberg.org/mychris/uvac"
  :build-operation "program-op"
  :build-pathname "uvac"
  :entry-point "uvac::plain-main"
  :in-order-to ((asdf:test-op (asdf:test-op "uvac/tests")))
  :depends-on (clingon
	       drakma
	       puri
	       plump
	       clss
	       cl-json
	       cl-ppcre
	       uiop)
  :components ((:module "src"
		:serial t
		:components
		((:file "utils")
		 (:file "ojudge")
		 (:file "uhunt")
		 (:file "udebug")
		 (:file "cmd-input")
		 (:file "cmd-accepted-output")
		 (:file "cmd-solved")
		 (:file "cmd-unsolved")
		 (:file "cmd-rank")
		 (:file "cmd-ranklist")
		 (:file "uvac"))))
  :description "")

(defsystem uvac/tests
  :author "Christoph Göttschkes"
  :license "ISC"
  :depends-on (uvac
	       fiveam)
  :components ((:module "tests"
		:serial t
		:components
		((:file "uvac-tests")
		 (:file "utils-tests")
		 (:file "uhunt-tests"))))
  :perform (asdf:test-op (op c)
			 (uiop:symbol-call :fiveam :run!
					   (uiop:find-symbol* :all-tests :uvac-tests)))
  :description "")
